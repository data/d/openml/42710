# OpenML dataset: shapes

https://www.openml.org/d/42710

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A subset of the 3D dataset from Princeton\'s COS 429 Computer Vision course. The dataset consists of 40 models organised into 4 classes of 10 objects each.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42710) of an [OpenML dataset](https://www.openml.org/d/42710). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42710/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42710/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42710/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

